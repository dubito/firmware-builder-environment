FROM debian:buster
ENV LINARO_VERSION 7.5.0-2019.12
ENV LINARO_HOST_ARCH x86_64
RUN apt-get update && apt-get -y install build-essential curl libssl-dev tcl git python bison flex bc device-tree-compiler parted dosfstools mtools u-boot-tools
RUN cd /tmp && curl -L https://releases.linaro.org/components/toolchain/binaries/latest-7/aarch64-linux-gnu/gcc-linaro-${LINARO_VERSION}-${LINARO_HOST_ARCH}_aarch64-linux-gnu.tar.xz -O && \
	mkdir -p /opt && cd /opt && tar -Jxvf /tmp/gcc-linaro-${LINARO_VERSION}-${LINARO_HOST_ARCH}_aarch64-linux-gnu.tar.xz
ENV CROSS_COMPILE /opt/gcc-linaro-${LINARO_VERSION}-${LINARO_HOST_ARCH}_aarch64-linux-gnu/bin/aarch64-linux-gnu-
RUN useradd -m build
USER build
